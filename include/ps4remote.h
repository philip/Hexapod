#pragma once

#include "IRemote.h"

#include <list>

class Ps4remote : public IRemote
{
public:

    Ps4remote();

    //! @param hostMAC The mac address to fake
    //!                This would normally be the address of the PS4
    //!                the controller is paired to
    Ps4remote(String hostMAC);

    ~Ps4remote() = default;

    void init() override;

    bool isConnected();

    const Output& output() override;

    void loop() override;

    void registerCallback(void(*callback)(const Output&)) override;

private:

    IRemote::Output _output;

    String _hostMac;

    std::list<void (*)(const IRemote::Output&)> _callbacks;

    void updateOutput();

};
#pragma once

//! @todo:
//! * Actual reading of battery
//! * Some simple calibration method e.g via terminal
//! * Callback registry for cutoff action

//! @brief Keep track of the battery status
class Vbat {
public:
    Vbat();

    float getCurrentVoltage() const;

    //! @return Charge in %
    float getCurrentCharge() const;
};
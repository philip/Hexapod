#pragma once

#include <Arduino.h>

class IRemote {
public:
    //! @brief Attitude is the "pose" of the Hexapod
    //!        without movement
    //!
    //! @arg roll/pitch   Tilt it around.    [-1, 1]
    //! @arg yaw          Twist it.          [-1, 1]
    //! @arg elevator     Lower or raise it. [-1, 1]
    typedef struct {
        float roll;
        float pitch;
        float yaw;
        float elevator;
    } Attitude;

    //! @brief Movement consists of the target velocity for
    //!
    //! @arg vx Vel back/forward. [-1, 1]
    //! @arg vy Vel left/right.   [-1, 1]
    //! @arg w  Rotational speed. [-1, 1]
    typedef struct {
        float vx;
        float vy;
        float w;
    } Movement;

    typedef struct {
        Attitude attitude;
        Movement movement;
        bool isNew;
    } Output;

    virtual const Output& output() = 0;

    virtual void init() = 0;
    
    virtual void loop() = 0;

    virtual void registerCallback(void (*callback)(const Output&)) = 0;
};
#pragma once
#include "IRemote.h"
#include "Vbat.h"

#include <WiFiUdp.h>

#include <list>

class OSCRemote : public IRemote {
public:
    //! Needs to be called in "Setup"
    OSCRemote(const Vbat&);

    void loop();

    const IRemote::Output& output() override;

    void registerCallback(void (*callback)(const IRemote::Output&)) override;

private:
    //! Place this after WiFi.begin() in main
    void init();

    IRemote::Output _output;

    std::list<void (*)(const IRemote::Output&)> _callbacks;

    const Vbat& _vbat;
};
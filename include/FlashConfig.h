#include "config.h"

#include <Arduino.h>
#include <EEPROM.h>

#include <array>

class FlashConfig {
public:
    typedef struct Data {
        std::array<char, 16> wifiSSID;
        std::array<char, 16> wifiPass;

        Data& operator=(const Data&);
    } Data;

    FlashConfig(){};

    void init();

    void load();

    void save();
    void save(Data&);

    void setSSID(String);
    void setWifiPass(String);

    const Data& data();

    void printConfig();

private:
    Data _data;
};

bool operator==(const FlashConfig::Data& a, const FlashConfig::Data& b);
bool operator!=(const FlashConfig::Data& a, const FlashConfig::Data& b);

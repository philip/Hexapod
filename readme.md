# The most awesome Hexapod
With hardware (3D printed parts) based on [Vorpal](https://www.vorpalrobotics.com/wiki/index.php?title=Vorpal_The_Hexapod) and software by Mattias Lasersköld and Philip Johansson.

## Hardware
The controller for this project is an [ESP32](https://www.espressif.com/en/products/hardware/esp32-devkitc/overview) which has WiFi and Bluetooth built in.
To drive the servos the [Adafruit PCA9685](https://learn.adafruit.com/16-channel-pwm-servo-driver?view=all) is used.
### Wiring
Use a 5V regulator of your choice. We are using the [Hobbywing 2-3S 8A (15A peak) UBEC](https://www.banggood.com/Hobbywing-2-3S-External-8A-MAX-15A-UBEC-Switch-Mode-p-1030910.html?rmmds=search&cur_warehouse=CN). This will power the motordriver and the ESP.
1. Connect the regulator to the input of the servo driver.
1. Connect ground and 5V from the servo driver to the ESP e.g. from an empty servo connector.
1. Connect the driver and the ESP through *I2C* with SDA to IO21 on the ESP and SCL to IO22.

## Software
Two files in this project have the extension .orig. This is so that they can be checked in with a *default* setting. These files need to be compied or moved to exclude this file extension. The files are:
* include/config.h.orig -> include/config.h
* include/settings.h.orig -> include/settings.h

**settings.h** contains settings most users will want to configure while **config.h** contains build specific flags and defines intended for developers.
  
### WiFi
WiFi settings reside in settings.h

### OSC (Open sound control) configuration
OSC settings reside in config.h

<!-- Not with platformIO?
### Dependencies
Some libraries needs to be installed before building:
* Adafruit PWM Servo Driver Library *by Adafruit*
* OSC *by Adrian Freed*
-->
## Remote alternatives
### OSC Touch
[OSCTouch](https://hexler.net/products/touchosc) *by Hexler* is an awesome tool for OSC & MIDI to use for mobile robot projects.
Install the desktop and mobile application for your platforms and sync the included layout to your mobile device.

### PS4 Controller
The PS4 controller can be connected using [this guide.](https://techtutorialsx.com/2020/02/15/esp32-connecting-a-ps4-controller/)
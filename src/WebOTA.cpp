/* --------------------------------------------- *
 * Code from http://esp32-server.de/webupdate/   *
 * --------------------------------------------- */

#include "WebOTA.h"
#include <Arduino.h>
#include <Update.h>
#include <WebServer.h>

WebServer* server;

const char* serverIndex = "\
    <form method='POST' action='/update' enctype='multipart/form-data'>\
      <input type='file' name='update'>\
      <input type='submit' value='Update'>\
    </form>";

WebOTA::WebOTA(int port) {
    server = new WebServer(port);
}

void WebOTA::setup() {

    auto rootHandlerFunc = []() {
        server->sendHeader("Connection", "close");
        server->send(200, "text/html", serverIndex);
    };
    auto updateHandlerFunc = []() {
        server->sendHeader("Connection", "close");
        server->send(200, "text/plain", (Update.hasError()) ? "NOK" : "OK");
        delay(1000);
        ESP.restart();
    };
    auto updateHandlerUFunc = [&]() {
        HTTPUpload& upload = server->upload();
        if (upload.status == UPLOAD_FILE_START) {
            Serial.setDebugOutput(true);
            Serial.printf("Update: %s\n", upload.filename.c_str());
            uint32_t maxSketchSpace = (1048576 - 0x1000) & 0xFFFFF000;
            if (!Update.begin(maxSketchSpace)) { // start with max available size
                Update.printError(Serial);
            }
        }
        else if (upload.status == UPLOAD_FILE_WRITE) {
            if (Update.write(upload.buf, upload.currentSize) != upload.currentSize) {
                Update.printError(Serial);
            }
        }
        else if (upload.status == UPLOAD_FILE_END) {
            if (Update.end(true)) { // true to set the size to the current progress
                Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
            }
            else {
                Update.printError(Serial);
            }
            Serial.setDebugOutput(false);
        }
        yield();
    };

    server->on("/", HTTP_GET, rootHandlerFunc);
    server->on("/update", HTTP_POST, updateHandlerFunc, updateHandlerUFunc);
    server->begin();
    Serial.println("HTTP Server started");
};

void WebOTA::loop() {
    server->handleClient();
}
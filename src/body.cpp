#include "body.h"

#include "config.h"

#include <Adafruit_PWMServoDriver.h>
#include <Arduino.h>
#include <SPI.h>
#include <Wire.h>

#include <cmath>

// Depending on your servo make, the pulse width min and max may vary, you
// want these to be as small/large as possible without hitting the hard stop
// for max range. You'll have to tweak them as necessary to match the servos you
// have!
#define SERVOMIN 190 // This is the 'minimum' pulse length count (out of 4096)
#define SERVOMAX 540 // This is the 'maximum' pulse length count (out of 4096)
#define USMIN                                                                                      \
    600 // This is the rounded 'minimum' microsecond length based on the minimum
        // pulse of 190
#define USMAX                                                                                      \
    2400              // This is the rounded 'maximum' microsecond length based on the
                      // maximum pulse of 540
#define SERVO_FREQ 60 // Analog servos run at ~50 Hz updates

Adafruit_PWMServoDriver servoDriver = Adafruit_PWMServoDriver(0x40);

unsigned long freqWatchDog = 0;
unsigned long SuppressScamperUntil = 0; // if we had to wake up the servos, suppress the power
                                        // hunger scamper mode for a while

void resetServoDriver() {
#ifndef CTRL_INACTIVE
    servoDriver.begin();
    servoDriver.setPWMFreq(SERVO_FREQ); // Analog servos run at ~60 Hz updates
#endif
}

void checkForServoSleep() {

#ifndef CTRL_INACTIVE
    if (millis() > freqWatchDog) {

        // See if the servo driver module went to sleep, probably due to a short
        // power dip
        Wire.beginTransmission(SERVO_IIC_ADDR);
        Wire.write(0); // address 0 is the MODE1 location of the servo driver,
                       // see documentation on the PCA9685 chip for more info
        Wire.endTransmission();
        Wire.requestFrom((uint8_t)SERVO_IIC_ADDR, (uint8_t)1);
        int mode1 = Wire.read();
        if (mode1 & 16) { // the fifth bit up from the bottom is 1 if controller
                          // was asleep
            // wake it up!
            resetServoDriver();
            // beep(1200,100);  // chirp to warn user of brown out on servo
            // controller
            SuppressScamperUntil =
                millis() + 10000; // no scamper for you! (for 10 seconds because we ran out
                                  // of power, give the battery a bit of time for charge
                                  // migration and let the servos cool down)
        }
        freqWatchDog = millis() + 100;
    }
#endif
}

float modifiedMap(float x, float in_min, float in_max, float out_min, float out_max) {
    float temp = (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    temp = (int)(4 * temp + .5);
    return (float)temp / 4;
}

Body::Body() 
: legs() 
{
    uint8_t numOfLegs = 6;
    for (uint8_t i = 0; i < numOfLegs; ++i) {
        legs.push_back(Leg(i, i + 6));
    }
}

void Body::init() {
    resetServoDriver();
}
void Body::healthCheck() {
    checkForServoSleep();
}

Leg::Leg(uint8_t hipp, uint8_t knee)
    : _joints({{Joint::Hipp, {.index = hipp, .trim = 0, .center = 0.0, .pos = 0}},
               {Joint::Knee, {.index = knee, .trim = 0, .center = pi/4, .pos = 0}}}) {
}

void Leg::setTrim(Joint j, float angle) {
    _joints[j].trim = angle;
}

void Leg::setPos(Joint j, float angle) {

    const float maxAngle = 1.43117; // both directions (visually observed)

    angle = std::min(std::max(angle, -maxAngle), maxAngle);

    servoDriver.writeMicroseconds(
        _joints[j].index, 
        modifiedMap(angle, -maxAngle, maxAngle, USMIN, USMAX) + _joints[j].center);

    _joints[j].pos = angle;
}
#include "OSCRemote.h"
#include "config.h"

#include <OSCBundle.h>
#include <OSCData.h>
#include <OSCMessage.h>

#include <array>

IRemote::Output* pOutput;

namespace {

WiFiUDP* pUdp;
IPAddress remoteIP;

// Scaling
//! @todo This is just temporary placement
const float rollFactor = 45;
const float pitchFactor = 45;
const float yawFactor = 45;
const float elevatorFactor = 0.1;
const float vxFactor = 0.1;
const float vyFactor = 0.1;
const float wFactor = 10;

void remoteMsgParser(OSCMessage& msg, int offset) {
    std::array<char, 20> topic;
    msg.getAddress(topic.data());
#ifdef DEBUG_OSC
    Serial.printf("%s [%s, %s]\n",
                  topic.data(),
                  String(msg.getFloat(0)).c_str(),
                  String(msg.getFloat(1)).c_str());
#endif

    if (!strcmp(topic.data(), "/att/xy")) {
        pOutput->attitude.pitch = msg.getFloat(0) * pitchFactor;
        pOutput->attitude.roll = msg.getFloat(1) * rollFactor;
    }
    else if (!strcmp(topic.data(), "/att/z")) {
        pOutput->attitude.yaw = msg.getFloat(0) * yawFactor;
    }
    else if (!strcmp(topic.data(), "/att/e")) {
        pOutput->attitude.elevator = msg.getFloat(0) * elevatorFactor;
    }
    else if (!strcmp(topic.data(), "/mov/xy")) {
        pOutput->movement.vy = msg.getFloat(0) * vxFactor;
        pOutput->movement.vx = msg.getFloat(1) * vyFactor;
    }
    else if (!strcmp(topic.data(), "/mov/z")) {
        pOutput->movement.w = msg.getFloat(0) * wFactor;
    }
    else {
        Serial.println("OSC Unkown topic");
    }
}

void remoteMsgParser(OSCMessage& msg) {
    remoteMsgParser(msg, 0);
}

String getOSCErrorName(OSCErrorCode error) {
    switch (error) {
    case OSC_OK:
        return String("OSC_OK");
    case BUFFER_FULL:
        return String("BUFFER_FULL");
    case INVALID_OSC:
        return String("INVALID_OSC");
    case ALLOCFAILED:
        return String("ALLOCFAILED");
    case INDEX_OUT_OF_BOUNDS:
        return String("INDEX_OUT_OF_BOUNDS");
    default:
        return String("UNKOWN ERROR");
    }
}

template <class T>
void msgOut(String topic, T data) {
    OSCMessage msg(topic.c_str());
    msg.add(data);
    pUdp->beginPacket(remoteIP, outPort);
    msg.send(*pUdp);   // send the bytes
    pUdp->endPacket(); // mark the end of the OSC Packet
    msg.empty();       // free space occupied by message
}

bool msgReceive() {
    OSCBundle msg;

    int size = pUdp->parsePacket();
    remoteIP = pUdp->remoteIP();
    if (size) {
        while (size--) {
            msg.fill(pUdp->read());
        }

#ifdef DEBUG_OSC
        Serial.printf("OSC packet from: %s\n", remoteIP.toString().c_str());
#endif

        if (!msg.hasError()) {
            msg.dispatch("/att/xy", remoteMsgParser);
            msg.dispatch("/att/e", remoteMsgParser);
            msg.dispatch("/att/z", remoteMsgParser);

            msg.dispatch("/mov/xy", remoteMsgParser);
            msg.dispatch("/mov/z", remoteMsgParser);
            return true;
        }
        else {
#ifdef DEBUG_OSC
            Serial.printf("error: %s\n", getOSCErrorName(msg.getError()).c_str());
#endif
        }
    }
    return false;
}
} // namespace

OSCRemote::OSCRemote(const Vbat& vbat) : _vbat(vbat) {
    init();
}

void OSCRemote::init() {
    pOutput = &_output;
    pUdp = new WiFiUDP();
    pUdp->begin(inPort);
}

void OSCRemote::loop() {
    if (msgReceive()) {
        for (auto c : _callbacks) {
            c(_output);
        }
    }

    unsigned long t = millis();
    static unsigned long tTrigger = t;
    const unsigned long updateInterval = 1000;
    if (t >= tTrigger) {
        // TODO: convert to string and append %
        char voltage[5]; // "100%\n"
        dtostrf(_vbat.getCurrentCharge(),
                3, // lenght [0, 100]
                0, // precision
                voltage);
        strcat(voltage, "%");
        msgOut("/label_bat", voltage);
        tTrigger += updateInterval;
    }
}

const IRemote::Output& OSCRemote::output() {
    return _output;
}

void OSCRemote::registerCallback(void (*callback)(const IRemote::Output&)) {
    _callbacks.push_back(callback);
}
#include "ps4remote.h"
#include "config.h"

#include <PS4Controller.h>

#include <Arduino.h>

#include <cmath>
#include <limits>

namespace {
void printValues(IRemote::Output& o)
{
    Serial.printf("Roll: %f, Pitch: %d \n", 
                  o.attitude.roll,
                  PS4.data.analog.stick.ry);
}
}

Ps4remote::Ps4remote()
: _output{
    .attitude = {},
    .movement = {},
    .isNew = true,
}
, _hostMac{}
{
}

Ps4remote::Ps4remote(String hostMAC)
: _output{
    .attitude = {},
    .movement = {},
    .isNew = true,
}
, _hostMac{hostMAC}
{
}

void Ps4remote::init()
{
    if (_hostMac == "")
        return;

    char c[_hostMac.length() + 1];
    strcpy(c, _hostMac.c_str());

    if (!PS4.begin(c))
    {
        Serial.println("Could not initialize PS4");
        return;
    }

    PS4.setLed(50, 255, 125);
}

void Ps4remote::registerCallback(void (*callback)(const IRemote::Output&)) {
    _callbacks.push_back(callback);
}

bool Ps4remote::isConnected()
{
    return PS4.isConnected();
}

const IRemote::Output& Ps4remote::output()
{
    return _output;
}

void Ps4remote::loop()
{
    updateOutput();
    for (auto c : _callbacks)
    {
        c(_output);
    }
}

void Ps4remote::updateOutput()
{
    if (!PS4.isConnected())
    {
        Serial.println("PS4 controller not connected");
        return;
    }
    auto norm = [](int8_t in) -> float
    {
        float t = static_cast<float>(in);
        return t / static_cast<float>(std::numeric_limits<int8_t>::max());
    };

    _output.attitude = {
        .roll = norm(PS4.data.analog.stick.rx),
        .pitch = norm(PS4.data.analog.stick.ry),
        .yaw = norm(PS4.data.analog.stick.lx),
        .elevator = norm(PS4.data.analog.stick.ly),
    };
    _output.isNew = true;

    float vx = norm(PS4.data.analog.button.r2 - PS4.data.analog.button.l2);
    vx = std::max(-1.0f, std::min(1.0f, vx));
    _output.movement.vx = 
        vx;
    _output.movement.vy = 0.0f;

    //printValues(_output);
}



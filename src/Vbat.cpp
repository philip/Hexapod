#include "Vbat.h"

Vbat::Vbat() {
}

float Vbat::getCurrentVoltage() const {
    return 4.12;
}

float Vbat::getCurrentCharge() const {
    return 87.0;
}
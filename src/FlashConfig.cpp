#include "FlashConfig.h"

void FlashConfig::init() {
    EEPROM.begin(EEPROM_SIZE);
}

void FlashConfig::setSSID(String ssid) {
    strcpy(_data.wifiSSID.data(), ssid.c_str());
};

void FlashConfig::setWifiPass(String pass) {
    strcpy(_data.wifiPass.data(), pass.c_str());
};

void FlashConfig::load() {
    byte* pBuff = (byte*)(const void*)&_data;
    for (size_t i = 0; i < sizeof(_data); ++i) {
        *pBuff++ = EEPROM.read(i);
    }
    Serial.println("EEPROM Loaded");
};

void FlashConfig::save() {
    const byte* pBuff = (const byte*)(const void*)&_data;
    for (size_t i = 0; i < sizeof(_data); ++i) {
        EEPROM.write(i, *pBuff++);
    }
    EEPROM.commit();
    Serial.println("EEPROM Saved");
};

void FlashConfig::save(FlashConfig::Data& data) {
    _data = data;
    save();
}

const FlashConfig::Data& FlashConfig::data() {
    return _data;
}

void FlashConfig::printConfig() {
    Serial.printf("wifiSSID: %s\n", _data.wifiSSID.data());
    Serial.printf("wifiPass: %s\n", _data.wifiPass.data());
}

FlashConfig::Data& FlashConfig::Data::operator=(const FlashConfig::Data& other) {
    this->wifiSSID = other.wifiSSID;
    this->wifiPass = other.wifiPass;
    return *this;
}

bool operator==(const FlashConfig::Data& a, const FlashConfig::Data& b) {
    return (a.wifiSSID == b.wifiSSID && a.wifiPass == b.wifiPass);
}
bool operator!=(const FlashConfig::Data& a, const FlashConfig::Data& b) {
    return !(a == b);
}